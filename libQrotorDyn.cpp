
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>

#include "matrix.h"
#include "libQrotorDyn.h"


//-- global variables
/*struct XC_NOISE_GAUSSIAN xc_noise_gaussian = { // sensor noise
	0,    2,  0,    2,  0,     2,  // position
	0,  0.1,  0,  0.1,  0,   0.1,  // velocity
	0, 0.02,  0, 0.02,  0,  0.02,  // attitude angle
	0, 0.01,  0, 0.01,  0,  0.01,  // angular rate
	0,    1,  0,    1,  0,     1,  // acceleration in the body frame
};*/



/*************************************************************************************
/*	Name:		Init
/*	Function:	Initialize the model and simulation				
/*	Parameter:	
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::Init()
{
	
	//-- Single Control Step, Multiple Simulation Steps
	m_ts = 0.005;			// step size for model simulation, sec
	m_Ts = 0.02;			// step size for control simulation, sec
	
	//-- the model parameters 
	g		= 9.781;        // gravitational acceleration, m/s^2
	m		= 4.027;        // total mass of helicopter, kg

	rho		= 1.292;        // air density, kg/m^3

	Ixx		= 0.0707;       // rolling moment of inertia, kg*m^2  (real measured value)
	Iyy		= 0.0705;       // pitching moment of inertia, kg*m^2 (scaled based on CIFER)
	Izz		= 0.2322;       // yawing moment of inertia, kg*m^2   (real measured value)

	Lm		= 0.35;         // the length of the arm of the platform
		
	Jr		= 0;            // rotor inertia
	OMG_r	= 0;			// rotation speed of the motor


	/*  configuration of the uav   
	// 
	//   3(cw)   1(ccw)
	//       \  / 
	//        \/
	//        /\   
	//       /  \
	//  2(ccw)   4(cw)
	*/

	//-- couter-clock-wise blade
	pwm2omg_ccw[0]	= 224.7527;
	pwm2omg_ccw[1]	=-461.6444; 
	pwm2omg_ccw[2]	= 529.3261;
	pwm2omg_ccw[3]	= 4.0788; 

	omg2Tz_ccw[0]	= 0.0005;
	omg2Tz_ccw[1]	=-0.0137;
	omg2Tz_ccw[2]	= 0.1698;

	omg2Rz_ccw[0]	= 0.000014942636333; 
	omg2Rz_ccw[1]	=-0.000607079456087; 
	omg2Rz_ccw[2]	= 0.009490512613259;

	//-- clock wise blade
	pwm2omg_cw[0]	= 249.2520;
	pwm2omg_cw[1]	=-496.6389; 
	pwm2omg_cw[2]	= 537.6168;
	pwm2omg_cw[3]	= 3.4888; 

	omg2Tz_cw[0]	= 0.0005;
	omg2Tz_cw[1]	=-0.0155;
	omg2Tz_cw[2]	= 0.2126;

	omg2Rz_cw[0]	= 0.000016000332401; 
	omg2Rz_cw[1]	=-0.000731438959227; 
	omg2Rz_cw[2]	= 0.011212137724635;


	//-- states and inputs
	for (int i=0; i<12; i++) m_xn[i]  = 0; 
	for (int i=0; i<15; i++) m_xc[i] = 0; 

	//-- acceleration
	for (int i=0; i<3; i++) m_a_b[i] = 0; 

	return 0;
}


/*************************************************************************************
/*	Name:		Init
/*	Function:	Initialize the model and simulation				
/*	Parameter:	
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::SetInitialState(double xn0[12], double a_b0[3])
{
	for (int i=0; i<12; i++)
	{
		m_xn[i]	= xn0[i]; 
	}

	m_a_b[0]	= a_b0[0];
	m_a_b[1]	= a_b0[1];
	m_a_b[2]	= a_b0[2];

	xn2xc(); 

	return 0;
}


/*************************************************************************************
/*	Name:		xn2xc
/*	Function:	add a_b and measuremnt noise		
/*	Parameter:	
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::xn2xc()               // convert xn to xc
{
	double *xn = m_xn;
	double *xc = m_xc; 

	double e[15]; 

	//-- generate the measurement noise
	for (int i=0; i<15; i++)
	{
		e[i] = 0;
	}

	//-- add the noises
	for (int i=0; i<12; i++)
	{
		xc[i] = xn[i] + e[i];
	}

	xc[12]	= m_a_b[0] + e[12];
	xc[13]	= m_a_b[1] + e[13];
	xc[14]	= m_a_b[2] + e[14];
	
	return 0;
}



/*************************************************************************************
/*	Name:		RunOneStep
/*	Function:	Single Step Simulation of Quadrotor				
/*	Parameter:	uin ---------- control inputs
/*	Return:		0 ------------ Succeed
/**************************************************************************************/
int QROTORDYN::RunOneStep(double uin[4])
{
	//-- get current states
	double xn[12];
	memcpy(xn, m_xn, sizeof(double)*12);

	//-- state update	
	int n = 12;             // the number of the states in the numerical integration
	double k1[12]; 
	double k2[12]; 
	double k3[12]; 
	double k4[12]; 
	double xk[12];			// a temporary matrix
	double wnd[3] = {0, 0, 0};

	double a_b[3];			// the load or poper acceleration measured by accelerometers
	double a_b_temp[3];		// temporary vector for proper acceleration
		
	//-- classical Runge Kutta method	
	for (int k = 0; k<m_Ts/m_ts; k++)
	{		
		if ( k==0 )
		{
			QrotorDynMod( xn, uin, wnd, k1, a_b );	
		}
		else 
		{
			QrotorDynMod( xn, uin, wnd, k1, a_b_temp );	
		}
				
		//-- xk = xn + k1 * (ts/2) ;
		for (int i=0; i<n; i++) xk[i] = xn[i] + k1[i] * m_ts /2.0; 
		QrotorDynMod( xk, uin, wnd, k2, a_b_temp );
		
		//-- xk = xn + k2 * (ts/2);
		for (int i=0; i<n; i++) xk[i] = xn[i] + k2[i] * m_ts /2.0; 
		QrotorDynMod( xk, uin, wnd, k3, a_b_temp );
		
		//-- xk = xn + k3 * ts;
		for (int i=0; i<n; i++) xk[i] = xn[i] + k3[i] * m_ts; 
		QrotorDynMod( xk, uin, wnd, k4, a_b_temp );
				
		for (int j=0; j<n; j++)  // xn = xn + ts/6 * ( k1 + 2*k2 + 2*k3 + k4 );
			xn[j] = xn[j] + (m_ts/6) * ( k1[j] + 2 * k2[j] + 2 * k3[j] + k4[j] );
		    
		//-- calculate dpsi, since psi is modeled using a first order system, dpsi is not a state in the linear model, which cannot be directly computed. 
		//-- We have to estimte it by using the differetiation of psi.
		//xn[11] = ( k1[8] + 2 * k2[8] + 2 * k3[8] + k4[8] ) / 6;
	}


	//-- update
	for (int i=0; i<12; i++)	m_xn[i]		= xn[i]; 
	for (int i=0; i<3;  i++)	m_a_b[i]	= a_b[i]; 
	//for (int i=0; i<4;  i++) m_uin[i] = uin[i];
	
	xn2xc(); 	

	return 0;
}



/*************************************************************************************
/*	Name:		QrotorDynMod
/*	Function:	Linear Dynamic Model of Quadrotor UAV
/*              Quadrotor with attitude stability augmentation 
/*  Parameter:
/*				x   = [pos; vel; att; omg] ------------ current system state
/*              pos = [x; y; z] ----------------------- current ground-axis position, m
                vel = [u; v; w] ----------------------- current body-axis velocity, m/s
				a_b = [abx, aby, abz] ----------------- current body-axis acceleration, m/s^2
				att = [phi; theta; psi] --------------- current attitude angles, rad
/*				u   ----------------------------------- the inputs to in [-1 1] without units
/*				wnd = [ wnd_u; wnd_v; wnd_w ] body-axis wind velocity as disturbance, m/s
/*              dx  delta of x
/*		
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::QrotorDynMod(double x[12], double u[4], double wnd[3], double dx[12], double a_b[3] )
{
	//-- Parse function inputs
	double vel_u	= x[3];       // m/s
	double vel_v	= x[4];       // m/s
	double vel_w	= x[5];       // m/s
	double phi		= x[6];       // rad
	double tht		= x[7];       // rad
	double psi		= x[8];       // rad
	double omg_p	= x[9];       // rad/s
	double omg_q	= x[10];      // rad/s
	double omg_r	= x[11];      // rad/s

	
	//-- rotation speed of the motors
	double Omg1		= pwm2omg_ccw[0] * pow(u[0],3) + 
					  pwm2omg_ccw[1] * pow(u[0],2) + 
				      pwm2omg_ccw[2] * u[0]        +
				      pwm2omg_ccw[3]; 

	double Omg2		= pwm2omg_ccw[0] * pow(u[1],3) + 
		              pwm2omg_ccw[1] * pow(u[1],2) + 
				      pwm2omg_ccw[2] * u[1]        +
				      pwm2omg_ccw[3]; 

	double Omg3		= pwm2omg_cw[0] * pow(u[2],3) + 
		              pwm2omg_cw[1] * pow(u[2],2) + 
				      pwm2omg_cw[2] * u[2]        +
				      pwm2omg_cw[3]; 

	double Omg4		= pwm2omg_cw[0] * pow(u[3],3) + 
		              pwm2omg_cw[1] * pow(u[3],2) + 
				      pwm2omg_cw[2] * u[3]        +
				      pwm2omg_cw[3]; 

	
	//-- Thurst of the motors
	double T_rt1	= omg2Tz_ccw[0] * pow(Omg1,2) + 
				      omg2Tz_ccw[1] * Omg1 + 
				      omg2Tz_ccw[2]; 

	double T_rt2	= omg2Tz_ccw[0] * pow(Omg2,2) + 
				      omg2Tz_ccw[1] * Omg2 + 
				      omg2Tz_ccw[2]; 

	double T_rt3	= omg2Tz_cw[0] * pow(Omg3,2) + 
					  omg2Tz_cw[1] * Omg3 + 
					  omg2Tz_cw[2]; 

	double T_rt4	= omg2Tz_cw[0] * pow(Omg4,2) + 
					  omg2Tz_cw[1] * Omg4 + 
					  omg2Tz_cw[2]; 


	//-- torques of the motors
	double Q_rt1	= omg2Rz_ccw[0] * pow(Omg1,2) + 
					  omg2Rz_ccw[1] * Omg1 + 
					  omg2Rz_ccw[2]; 

	double Q_rt2	= omg2Rz_ccw[0] * pow(Omg2,2) + 
					  omg2Rz_ccw[1] * Omg2 + 
					  omg2Rz_ccw[2]; 

	double Q_rt3	= omg2Rz_cw[0] * pow(Omg3,2) + 
					  omg2Rz_cw[1] * Omg3 + 
					  omg2Rz_cw[2]; 

	double Q_rt4	= omg2Rz_cw[0] * pow(Omg4,2) + 
					  omg2Rz_cw[1] * Omg4 + 
					  omg2Rz_cw[2]; 


	// compute rotor force and moments
	double Xrt		=  0;
	double Yrt		=  0; 
	double Zrt		= -(T_rt1 + T_rt2 + T_rt3 + T_rt4); 

	double Lrt		= Lm / sqrt(2.0) * ( -T_rt1 + T_rt2 + T_rt3 - T_rt4 );
	double Mrt		= Lm / sqrt(2.0) * (  T_rt1 - T_rt2 + T_rt3 - T_rt4 );
	double Nrt		= Q_rt1 + Q_rt2 - Q_rt3 - Q_rt4;
	
	
	// forces and moments along body axes
	double F_u		= Xrt;
	double F_v		= Yrt;
	double F_w		= Zrt;
	double M_p		= Lrt;
	double M_q		= Mrt;
	double M_r		= Nrt;


	// Rigid body dynamics
	double domg_p	= ( ( Iyy - Izz ) * omg_q * omg_r + Jr * omg_q * OMG_r + M_p ) / Ixx; // body gyro effect + propeller gyro effect + roll actuator action
	double domg_q	= ( ( Ixx - Izz ) * omg_p * omg_r + Jr * omg_p * OMG_r + M_q ) / Iyy;
	double domg_r	= ( ( Ixx - Iyy ) * omg_p * omg_q + M_r) / Izz;

	double dphi		= omg_p + sin(phi)*tan(tht)*omg_q + cos(phi)*tan(tht)*omg_r;
	double dtht		=                  cos(phi)*omg_q +         -sin(phi)*omg_r; 
	double dpsi		=         sin(phi)/cos(tht)*omg_q + cos(phi)/cos(tht)*omg_r;

	clsMatrix _R( 3, 3, 0, 0 );	// rotation matrix from body coordinates to ground coordinates
	Body2NED( phi, tht, psi, _R);	

	double a_u		= F_u / m - g * sin(tht);
	double a_v		= F_v / m + g * sin(phi)*cos(tht);
	double a_w		= F_w / m + g * cos(phi)*cos(tht);

	double dvel_u	= a_u - omg_q*vel_w + omg_r*vel_v;
	double dvel_v	= a_v - omg_r*vel_u + omg_p*vel_w;
	double dvel_w	= a_w - omg_p*vel_v + omg_q*vel_u;

	double v_b[3]	= {vel_u, vel_v, vel_w};
	clsMatrix _v_b(3, 1, v_b, 0); 

	clsMatrix _v_g(3, 1, 0, 0); 
	clsMatrix::X(_R, _v_b, _v_g);

	double *v_g		= _v_g.GetP();
	
	double dpos_x	= v_g[0];
	double dpos_y	= v_g[1];
	double dpos_z	= v_g[2]; 
	
	//-- outputs
	dx[0]			= dpos_x;		
	dx[1]			= dpos_y;		
	dx[2]			= dpos_z;	 
	dx[3]			= dvel_u;		
	dx[4]			= dvel_v;		
	dx[5]			= dvel_w;	
	dx[6]			= dphi;		
	dx[7]			= dtht;		
	dx[8]			= dpsi;	
	dx[9]			= domg_p;	    
	dx[10]			= domg_q;	    
	dx[11]			= domg_r;
	
	a_b[0]			= F_u / m;
	a_b[1]			= F_v / m;
	a_b[2]			= F_w / m;

	return 0;
}




/*************************************************************************************
/*	Name:		Setfxc_noise
/*	Function:	set the fxc and xc using xn + noise			
/*	Parameter:	
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::Setfxc_noise()               // convert xn to xc
{
	double *xn = m_xn;
	double *xc = m_xc; 

	double e[15]; 

	for (int i=0; i<15; i++)
	{
		e[i] = 0;
	}

	for (int i=0; i<15; i++)
	{
		xc[i] = xn[i] + e[i];
	}


	/*std::normal_distribution<double> nd_p_x(xc_noise_gaussian.p_x_mu, xc_noise_gaussian.p_x_sigma);
	std::normal_distribution<double> nd_p_y(xc_noise_gaussian.p_y_mu, xc_noise_gaussian.p_y_sigma);
	std::normal_distribution<double> nd_p_z(xc_noise_gaussian.p_z_mu, xc_noise_gaussian.p_z_sigma);

	std::normal_distribution<double> nd_v_u(xc_noise_gaussian.v_u_mu, xc_noise_gaussian.v_u_sigma);
	std::normal_distribution<double> nd_v_v(xc_noise_gaussian.v_v_mu, xc_noise_gaussian.v_v_sigma);
	std::normal_distribution<double> nd_v_w(xc_noise_gaussian.v_w_mu, xc_noise_gaussian.v_w_sigma);

	std::normal_distribution<double> nd_phi(xc_noise_gaussian.phi_mu, xc_noise_gaussian.phi_sigma);
	std::normal_distribution<double> nd_tht(xc_noise_gaussian.tht_mu, xc_noise_gaussian.tht_sigma);
	std::normal_distribution<double> nd_psi(xc_noise_gaussian.psi_mu, xc_noise_gaussian.psi_sigma);

	std::normal_distribution<double> nd_w_p(xc_noise_gaussian.w_p_mu, xc_noise_gaussian.w_p_sigma);
	std::normal_distribution<double> nd_w_q(xc_noise_gaussian.w_q_mu, xc_noise_gaussian.w_q_sigma);
	std::normal_distribution<double> nd_w_r(xc_noise_gaussian.w_r_mu, xc_noise_gaussian.w_r_sigma);

	std::normal_distribution<double> nd_abx(xc_noise_gaussian.abx_mu, xc_noise_gaussian.abx_sigma);
	std::normal_distribution<double> nd_aby(xc_noise_gaussian.aby_mu, xc_noise_gaussian.aby_sigma);
	std::normal_distribution<double> nd_abz(xc_noise_gaussian.abz_mu, xc_noise_gaussian.abz_sigma);

	e[0] = nd_p_x(m_generator);
	e[1] = nd_p_y(m_generator);
	e[2] = nd_p_z(m_generator);

	e[3] = nd_v_u(m_generator);
	e[4] = nd_v_v(m_generator);
	e[5] = nd_v_w(m_generator);

	e[6] = nd_phi(m_generator);
	e[7] = nd_tht(m_generator);
	e[8] = nd_psi(m_generator);

	e[9]  = nd_w_p(m_generator);
	e[10] = nd_w_q(m_generator);
	e[11] = nd_w_r(m_generator);

	e[12] = nd_abx(m_generator);
	e[13] = nd_aby(m_generator);
	e[14] = nd_abz(m_generator);*/

	
	/*
	//-- position, velocity, attitude angles
	for (int i=0; i<9; i++)
		xc[i] = xn[i] + e[i];

	//-- calculate angular rate: omg = S * datt
	double phi = xn[6];
	double tht = xn[7];


	clsMatrix _S(3, 3, 0, 0);
	SS(phi, tht, _S);

	double datt[3] = {xn[9], xn[10], xn[11]};
	clsMatrix _datt( 3, 1, datt, 0); 
	clsMatrix _omg( 3, 1, 0, 0);
	clsMatrix::X( _S, _datt, _omg);

	double *omg = _omg.GetP();
	xc[9]  = omg[0] + e[9];
	xc[10] = omg[1] + e[10];
	xc[11] = omg[2] + e[11];

	//-- acceleration in the body axis
	xc[12] = xn[16] + e[12];
	xc[13] = xn[17] + e[13];
	xc[14] = xn[18] + e[14];

	//-- update
	xc2fxc(m_xc, m_fxc);
	*/

	return 0;
}


/*************************************************************************************
/*	Name:		Body2NED
/*	Function:	rotation matrix transforming body-axis vector to NED-axis vector				
/*	Parameter:	[phi, tht, psi]  -------- input Euler angles
/*              R  ---------------------- output rotation matrix
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::Body2NED(double phi, double tht, double psi, clsMatrix &R)
{
	int m = R.GetM(); // get the rows
	int n = R.GetN(); // get the columns

	if ( m!=3 || n!=3 ){	printf("The dimension of R_b2g is not correct!"); return 1;	}

	double *p = R.GetP(); 
	*(p + 0 * 3 + 0) = cos(psi)*cos(tht);
	*(p + 0 * 3 + 1) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
	*(p + 0 * 3 + 2) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
	*(p + 1 * 3 + 0) = sin(psi)*cos(tht);
	*(p + 1 * 3 + 1) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
	*(p + 1 * 3 + 2) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
	*(p + 2 * 3 + 0) =-sin(tht);
	*(p + 2 * 3 + 1) = cos(tht)*sin(phi);
	*(p + 2 * 3 + 2) = cos(tht)*cos(phi);

	return 0;
}


/*************************************************************************************
/*	Name:		SS
/*	Function:	lumped transformation matrix for rotation kinematic, (Cai's book, pp. 33) 			
/*	Parameter:	
/*              phi ----------- roll angle 
/*              tht ----------- pitch angle
/*              SS ------------ the lumped transformation matrix
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORDYN::SS(double phi, double tht, clsMatrix &SS)
{
	double *pSS = SS.GetP();  
	pSS[0] = 1;
	pSS[1] = 0;
	pSS[2] =-sin(tht);
	pSS[3] = 0;
    pSS[4] = cos(phi);
	pSS[5] =-sin(phi)*cos(tht);
	pSS[6] = 0;
	pSS[7] =-sin(phi);
	pSS[8] = cos(phi)*cos(tht);

	return 0;
}




/*************************************************************************************
/*	Name:		Setfxc
/*	Function:	set the fxc and xc using xn			
/*	Parameter:	
/*	Return:		0 -- Succeed
///**************************************************************************************/
//int QROTORDYN::Setfxc()               // convert xn to xc
//{
//	double *xn = m_xn;
//	double *xc = m_xc; 
//
//	//-- position, velocity, attitude angles
//	for (int i=0; i<9; i++)
//		xc[i] = xn[i];
//
//	//-- calculate angular rate: omg = S * datt
//	double phi = xn[6];
//	double tht = xn[7];
//
//	/*Mat S = Mat::zeros(3, 3, CV_64F);
//	SS(phi, tht, S);
//
//	Mat datt = ( Mat_<double>(3,1) << xn.at<double>(9), xn.at<double>(10), xn.at<double>(11) );
//	Mat omg  = S * datt; */
//
//	clsMatrix _S(3, 3, 0, 0);
//	SS(phi, tht, _S);
//
//	double datt[3] = {xn[9], xn[10], xn[11]};
//	clsMatrix _datt( 3, 1, datt, 0); 
//	clsMatrix _omg( 3, 1, 0, 0);
//	clsMatrix::X( _S, _datt, _omg);
//
//	double *omg = _omg.GetP();
//	xc[9]		= omg[0];
//	xc[10]		= omg[1];
//	xc[11]		= omg[2];
//
//	xc[12]		= xn[16];
//	xc[13]		= xn[17];
//	xc[14]		= xn[18];
//
///*	double _datt[3], _omg[3], _S[3][3];
//	for (int i=0; i<3; i++) _datt[i] = datt.at<double>(i);
//	for (int i=0; i<3; i++) _omg[i]  =  omg.at<double>(i);
//	for (int i=0; i<3; i++) for (int j=0; j<3; j++) _S[i][j] = S.at<double>(i,j);
//	*/
//
//	//-- update
//	//xc2fxc(m_xc, m_fxc);
//
//	return 0;
//}
