clc; close all; clear; 
%-- read the output results
y_c = importdata('output.txt'); 
ts  = 0.02; 
tc  = 0:ts:(length(y_c)-1)*ts; 


% calculate ug, vg, wg (velocity in NED frame) and ag
R = eye(3,3); % rotation matrix transforming body-axis vector to ned-axis vector
Vg_c = zeros(3, length(y_c));
ag_c = zeros(3, length(y_c));
for i=1:length(y_c)
    phi = y_c(i,7);
    tht = y_c(i,8);
    psi = y_c(i,9);
    
    R = R_Body2NED(phi, tht, psi);    
    v_u_c = y_c(i,4);
    v_v_c = y_c(i,5);
    v_w_c = y_c(i,6);
    
    Vg_c(:,i) = R*[v_u_c; v_v_c; v_w_c];
    
    %ag_c(:,i) = R*a_b_all(i,:)'; 
end


%% plot

figure(1)

% position
subplot(3,2,1); plot(tc, y_c(:,1), 'b-.', 'linewidth', 2); ylabel('x (m)'); grid on; %legend('Simulink', 'M-file', 'C code');
subplot(3,2,3); plot(tc, y_c(:,2), 'b-.', 'linewidth', 2); ylabel('y (m)'); grid on;
subplot(3,2,5); plot(tc, y_c(:,3), 'b-.', 'linewidth', 2); ylabel('z (m)'); grid on;

% velocity
subplot(3,2,2); plot(tc, Vg_c(1,:), 'b-.', 'linewidth', 2); ylabel('v_x (m/s)'); grid on;
subplot(3,2,4); plot(tc, Vg_c(2,:), 'b-.', 'linewidth', 2); ylabel('v_y (m/s)'); grid on;
subplot(3,2,6); plot(tc, Vg_c(3,:), 'b-.', 'linewidth', 2); ylabel('v_z (m/s)'); grid on;


figure(2)

% Euler angle
subplot(3,2,1); plot(tc, y_c(:,7), 'b-.', 'linewidth', 2); ylabel('\phi (rad)'); grid on; %legend('Simulink', 'M-file', 'C code');
subplot(3,2,3); plot(tc, y_c(:,8), 'b-.', 'linewidth', 2); ylabel('\theta (rad)'); grid on;
subplot(3,2,5); plot(tc, y_c(:,9), 'b-.', 'linewidth', 2); ylabel('\psi (rad)'); grid on;

% derivative of Euler angle
subplot(3,2,2); plot(tc, y_c(:,10), 'b-.', 'linewidth', 2); ylabel('p (rad/s)'); grid on;
subplot(3,2,4); plot(tc, y_c(:,11), 'b-.', 'linewidth', 2); ylabel('q (rad/s)'); grid on;
subplot(3,2,6); plot(tc, y_c(:,12), 'b-.', 'linewidth', 2); ylabel('r (rad/s)'); grid on;