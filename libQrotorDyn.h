/*
 * libQrotorDyn.h
 *
 *  Created on: 07-25-2017
 *      Author: Dr. Lin Feng
 *
 * Coded by Dr. Lin Feng    Modeled by Dr. Peng Kemao and Pang Tao    Directed by Prof. Ben M. Chen
 * Copyright 2017 research UAV team, National University of Singapore
 * Updated: July 2017
 * 
 * It is used to simulate the dynamics of the quadrotor
 *
*/

#ifndef DEFQROTORDYN_
#define DEFQROTORDYN_


#include "matrix.h"


/*
struct XC_NOISE_GAUSSIAN         
{
	double p_x_mu;			// mean
	double p_x_sigma;       // standard deviation
	double p_y_mu;
	double p_y_sigma;
	double p_z_mu;
	double p_z_sigma; 

	double v_u_mu;	
	double v_u_sigma;
	double v_v_mu;
	double v_v_sigma;
	double v_w_mu;
	double v_w_sigma;

	double phi_mu;
	double phi_sigma;
	double tht_mu;
	double tht_sigma;
	double psi_mu;
	double psi_sigma;

	double w_p_mu;
	double w_p_sigma;
	double w_q_mu;
	double w_q_sigma;
	double w_r_mu;
	double w_r_sigma;

	double abx_mu;
	double abx_sigma;
	double aby_mu;
	double aby_sigma;
	double abz_mu;
	double abz_sigma;
};
*/

class QROTORDYN {

public:	

	QROTORDYN(){ Init(); };
	~QROTORDYN(){};

	int Init();													// initialize the class	
	int SetInitialState(double xn0[12], double a_b[3]);				// set the initial states of the UAV
	int RunOneStep(double uin[4]);								// run one step

public:
	

	//int Setfxc();  // set the fxc and xc using xn  
	//int SetSensorNoise( struct XC_NOISE_GAUSSIAN noise );		// set the parameters of the sensor noise
	int Setfxc_noise();											// set the fxc and xc using xn + noise
	//int Setfxc_external(struct FMT_XC fxc);                     // set the fxc and xc using external measurements, e.g. SLAM


		
	double m_xc[15];			/* xc  = [pos; v_b; att; omg; a_b] ---- current measured system state for control   
								 * pos = [x; y; z] -------------------- current ground-axis position, m
								 * vel = [u; v; w] -------------------- current body-axis velocity, m/s
								 * att = [phi; tht; psi] -------------- current attitude angles, rad
								 * omg = [p; q; r] -------------------- current angular rates, rad/s
								 * a_b = [abx; aby; abz] -------------- current proper/load acceleration measured by IMU, m/s^2
								 * 
								 * compared to 'xn', we add noise and proper acceleration in 'xc'
								 * 
								 */

protected:
	int xn2xc();				// generate xc based on xn


protected:
	int QrotorDynMod(double x[12], double u[4], double wnd[3], double dx[12], double a_b[3] );		// the dynamic model 
	int Body2NED(double phi, double tht, double psi, clsMatrix &R);					// rotation matrix transforming body-axis vector to NED-axis vector

	int SS(double phi, double tht, clsMatrix &SS);				// lumped transformation matrix for rotation kinematic


protected:	
	//-- inputs, outputs and states
	double m_xn[12];			/* xn  = [pos; v_b; att; omg] --------- current model states including unmeasured states 
								 * pos = [x; y; z] -------------------- current ground-axis position, m
								 * vel = [u; v; w] -------------------- current body-axis velocity, m/s
								 * att = [phi; tht; psi] -------------- current attitude angles, rad
								 * omg = [p; q, r] -------------------- current derivatives of attitude angles, rad/s
								 */

	double m_a_b[3];            // the load or poper acceleration measured by accelerometers, m/s^2


	//-- simulation parameters
	double m_ts;					// step size for model simulation, sec
	double m_Ts;					// step size for control simulation, sec


protected:

	//-- model paramters
	double g;        // gravitational acceleration, m/s^2
	double m;        // total mass of helicopter, kg

	double rho;      // air density, kg/m^3

	double Ixx;      // rolling moment of inertia, kg*m^2  (real measured value)
	double Iyy;      // pitching moment of inertia, kg*m^2 (scaled based on CIFER)
	double Izz;      // yawing moment of inertia, kg*m^2   (real measured value)

	double Lm;       // the length of the arm of the platform

	double Jr;       // rotor inertia
	double OMG_r;    // rotation speed of the motor

	//-- counter-clock-wise blade
    double pwm2omg_ccw[4];		// pwm to rotation speed
	double omg2Tz_ccw[3];		// rotation speed to thrust
	double omg2Rz_ccw[3];       // rotation speed to torque

	//-- clock-wise blade
    double pwm2omg_cw[4];		// pwm to rotation speed
	double omg2Tz_cw[3];		// rotation speed to thrust
	double omg2Rz_cw[3];       // rotation speed to torque
	
	//-- noise generation
	//std::default_random_engine m_generator;
	
};


#endif