/* 
 * libQrotorCtrl.h
 *
 *  Created on: 25-07-2017
 *      Author: Lin Feng
 *
 */
//#include "stdafx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <stdio.h>

//#include "matrix.h"
#include "libQrotorCtrl.h"


/*************************************************************************************
/*	Name:		Init
/*	Function:	Initialize the model and simulation				
/*	Parameter:	
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORCTRL::Init()
{
	//-- simulation parameters
	
	//-- Single Control Step, Multiple Simulation Steps
	m_ts		= 0.005;		// step size for model simulation, sec
	m_Ts		= 0.02;			// step size for control simulation, sec

	//-- Model parameters
	g			= 9.781;        // gravitational acceleration in Singapore, m/s^2
	m			= 4.027;        // total mass of helicopter, kg

	Kt1			= 0.0005;       // Thrust coefficient, counter clock wise
	Kt2			= 0.0005;       // Thrust coefficient, clock wise
	Kq			= 0.000013;     // Drag coefficient

	Kw1[0]		= 283.3097;     // the gain of PWM to ration speed, counter clock wise. 
	Kw1[1]		= 28.7300;		// the gain of PWM to ration speed, counter clock wise. 

	Kw2[0]		= 278.5778;		// the gain of PWM to ration speed, clock wise
	Kw2[1]		= 28.9878;		// the gain of PWM to ration speed, clock wise

	Lm			= 0.35;			// the length of the arm of the platform, m

	Ixx			= 0.0707;		// rolling moment of inertia, kg*m^2  (real measured value)
	Iyy			= 0.0705;		// pitching moment of inertia, kg*m^2 (ream measured value)
	Izz			= 0.2322;		// yawing moment of inertia, kg*m^2   (real measured value)

	//-- ESC control
	omega2_u << -Lm*Kt1/sqrt(2.0),  Lm*Kt1/sqrt(2.0),  Lm*Kt2/sqrt(2.0),  -Lm*Kt2/sqrt(2.0),
				 Lm*Kt1/sqrt(2.0), -Lm*Kt1/sqrt(2.0),  Lm*Kt2/sqrt(2.0),  -Lm*Kt2/sqrt(2.0),
				               Kq,                Kq,               -Kq,                -Kq,
				             -Kt1,              -Kt1,              -Kt2,               -Kt2;      
	
	u_omega2	= omega2_u.inverse();


	//-- Initialize the control law
	
	// attitude control law
	ksi_phi		= 0.8; 
	wn_phi		= 2 * 1.55 * M_PI *3;	// 1.55 Hz bandwidth for the Blacklion 168
	F_phi[0]	=-wn_phi*wn_phi;
	F_phi[1]	=-2*ksi_phi*wn_phi;
	G_phi		=-F_phi[0];
 
	ksi_tht		= 0.8; 
	wn_tht		= 2 * 1.55 * M_PI * 3; // 1.55 Hz bandwidth for the Blacklion 168
	F_tht[0]	=-wn_tht*wn_tht;
	F_tht[1]	=-2*ksi_tht*wn_tht;
	G_tht		=-F_tht[0];

	// xy outer-loop controller
	wn_xy		= 0.4;
	sigma_xy	= 1.1;
	ki_xy		= 0.8;
	eps_xy		= 1 * 2.0;
	F_xy[0]		= ( wn_xy * wn_xy        + 2 * sigma_xy * wn_xy * ki_xy ) / pow(eps_xy,2);
	F_xy[1]		= ( 2 * sigma_xy * wn_xy + ki_xy ) / eps_xy;
	F_xy[2]		=-ki_xy * wn_xy * wn_xy / pow(eps_xy,3);
	F_xy[3]		=-( wn_xy * wn_xy        + 2 * sigma_xy * wn_xy * ki_xy ) / pow(eps_xy,2);
	F_xy[4]		=-( 2 * sigma_xy * wn_xy + ki_xy ) / eps_xy;

	// z outer-loop controller
	wn_z		= 0.7;
	sigma_z		= 1.1;
	ki_z		= 0.8;
	eps_z		= 1;
	F_z[0]		= (wn_z*wn_z+2*sigma_z*wn_z*ki_z)/pow(eps_z,2);
	F_z[1]		= (2*sigma_z*wn_z+ki_z)/eps_z; 
	F_z[2]		=-ki_z*wn_z*wn_z/pow(eps_z,3); 
	F_z[3]		=-(wn_z*wn_z+2*sigma_z*wn_z*ki_z)/pow(eps_z,2);
	F_z[4]		=-(2*sigma_z*wn_z+ki_z)/eps_z;

	// psi controller design
	ksi_psi		= 0.8; 
	wn_psi		= 2 * 0.5 * M_PI * 2;  // 1.55 Hz bandwidth for the Blacklion 168
	F_psi[0]	=-wn_psi*wn_psi;
	F_psi[1]	=-2*ksi_psi*wn_psi;
	G_psi	    =-F_psi[0];

	//-- integrator
	for (int i=0; i<3; i++) ipos_err[i]   = 0; 
	ipsi_err = 0;

	return 0;
}

/*************************************************************************************
/*	Name:		Body2NED
/*	Function:	rotation matrix transforming body-axis vector to NED-axis vector				
/*	Parameter:	[phi, tht, psi]  -------- input Euler angles
/*              R  ---------------------- output rotation matrix
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORCTRL::Body2NED(double phi, double tht, double psi, clsMatrix &R)
{
	int m = R.GetM(); // get the rows
	int n = R.GetN(); // get the columns

	if ( m!=3 || n!=3 ){	printf("The dimension of R_b2g is not correct!"); return 1;	}

	double *p = R.GetP(); 
	*(p + 0 * 3 + 0) = cos(psi)*cos(tht);
	*(p + 0 * 3 + 1) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
	*(p + 0 * 3 + 2) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
	*(p + 1 * 3 + 0) = sin(psi)*cos(tht);
	*(p + 1 * 3 + 1) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
	*(p + 1 * 3 + 2) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
	*(p + 2 * 3 + 0) =-sin(tht);
	*(p + 2 * 3 + 1) = cos(tht)*sin(phi);
	*(p + 2 * 3 + 2) = cos(tht)*cos(phi);

	return 0;
}


/*************************************************************************************
/*	Name:		matrix_from_euler
/*	Function:	rotation matrix transforming body-axis vector to NED-axis vector				
/*	Parameter:	[phi, tht, psi]  -------- input Euler angles
/*              R  ---------------------- output rotation matrix
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORCTRL::matrix_from_euler(double phi, double tht, double psi, Eigen::Matrix3d &R)
{
	int m	= R.rows(); // get the rows
	int n	= R.cols(); // get the columns

	if ( m!=3 || n!=3 ){	printf("The dimension of R_b2g is not correct!"); return 1;	}

	R(0,0)	= cos(psi)*cos(tht);
	R(0,1)	= cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
	R(0,2)	= cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
	R(1,0)	= sin(psi)*cos(tht);
	R(1,1)	= sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
	R(1,2)	= sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
	R(2,0)	=-sin(tht);
	R(2,1)	= cos(tht)*sin(phi);
	R(2,2)	= cos(tht)*cos(phi);

	return 0;
}

/*************************************************************************************
/*	Name:		matrix_to_euler
/*	Function:	compute the Euler angles from rotation matrix transforming body-axis vector to NED-axis vector				
/*	Parameter:	R  ---------------------- intput rotation matrix
/*              [phi, tht, psi]  -------- outputput Euler angles
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORCTRL::matrix_to_euler(Eigen::Matrix3d R, double &phi, double &tht, double &psi )  // euler = [roll; pitch; yaw]
{
	double euler[3] = {0,0,0};

	euler[1] = asin( -R(2,0) );

	if ( abs( euler[1] - M_PI/2.0 ) < 1.0e-3 ) 
	{
		euler[0] = 0.0;
		euler[2] = atan2( R(1,2) - R(0,1), R(0,2) + R(1,1) ) + euler[0];
	}
	else if ( abs( euler[1] + M_PI/2.0 ) < 1.0e-3 ) 
	{
		euler[0] = 0.0;
		euler[2] = atan2( R(1,2) - R(0,1), R(0,2) + R(1,1) ) - euler[0];
	}	 
	else 
	{
		euler[0] = atan2( R(2,1), R(2,2) );
		euler[2] = atan2( R(1,0), R(0,0) );
	}

	phi = euler[0];
	tht = euler[1];
	psi = euler[2]; 

	return 0;
}



/*************************************************************************************
/*	Name:		RunOneStep
/*	Function:	Single Step Simulation of Quadrotor				
/*	Parameter:	
				ref = [pos_r; vel_r; acc_r, psi_r; r_r] --- current reference input
				       pos_r ------------------------------ desired ground position, m/s
				       vel_r ------------------------------ desired ground velocity, m/s
				       acc_r ------------------------------ desired ground acceleration, m/s^2
				       psi_r ------------------------------ desired heading, rad
				       r_r   ------------------------------ desired heading rate, rad/s
				Itg = [ipos_err; ipsi_err]----------------- the integration error of the
				                                            position and  psi, temperory 
				                                            values
/*	Return:		0 ------------ Succeed
/**************************************************************************************/
int QROTORCTRL::RunOneStep(QROTOR_REF ref, double xc[15])
{
	// Inputs
	double pos_r[3] = {ref.p_x_r, ref.p_y_r, ref.p_z_r};
	double vel_r[3] = {ref.v_x_r, ref.v_y_r, ref.v_z_r};
	double acc_r[3] = {ref.agx_r, ref.agy_r, ref.agz_r};
	double psi_r	= ref.psi_r;
	double r_r		= ref.r_r; 
	psi_r			= psi_r - floor( (psi_r + M_PI) / ( 2 * M_PI) ) * ( 2 * M_PI); // convert to [-pi pi]

	// Operating Point
	/*vel0 = [0; 0; 0];
	att0 = [0; 0; 0];
	omg0 = [0; 0; 0];
	xun0 = [0; 0; 0];
	uin0 = [0; 0; 0; 0];
	wnd0 = [0; 0; 0];*/

	// current states
	double pos[3]	= {xc[0], xc[1], xc[2]};
	double v_b[3]	= {xc[3], xc[4], xc[5]};
	double phi		= xc[6] - floor( ( xc[6]+M_PI ) / (2 * M_PI) ) * ( 2*M_PI );  // convert to [-pi pi]
	double tht		= xc[7] - floor( ( xc[7]+M_PI ) / (2 * M_PI) ) * ( 2*M_PI );
	double psi		= xc[8] - floor( ( xc[8]+M_PI ) / (2 * M_PI) ) * ( 2*M_PI );
	double omg[3]	= {xc[9], xc[10], xc[11]};

	Matrix3d _R = Matrix3d::Identity(); // rotation matrix from body coordinates to ground coordinates
	matrix_from_euler(phi, tht, psi, _R);

	Vector3d _v_b(v_b); 
	Vector3d _v_g = Vector3d::Zero();
	_v_g = _R * _v_b; 

	double v_g[3];
	v_g[0] = _v_g(0); 
	v_g[1] = _v_g(1);
	v_g[2] = _v_g(2);

	// outer-loop control
	for (int i=0; i<3; i++)
	{
		ipos_err[i] = ipos_err[i] + m_Ts * ( pos_r[i] - pos[i] ); 
	}

	double thr_ipos_err = 10;	// threshold
	for (int i=0; i<3; i++)
	{
		if (ipos_err[i] <-thr_ipos_err) ipos_err[i] =-thr_ipos_err;
		if (ipos_err[i] > thr_ipos_err) ipos_err[i] = thr_ipos_err;
	}

	double psi_err  = ( psi_r - psi ); // the error of psi_r and psi
	psi_err			= psi_err - floor( (psi_err + M_PI) / (2*M_PI) ) * (2 *M_PI); // convert to [-pi pi]
	
	ipsi_err		= ipsi_err + m_Ts * psi_err; 
	double thr_ipsi_err = 0.3; 
	if (ipsi_err <-thr_ipsi_err) ipsi_err =-thr_ipsi_err; 
	if (ipsi_err > thr_ipsi_err) ipsi_err = thr_ipsi_err; 

	ipsi_err		= ipsi_err - floor( (ipsi_err+M_PI) / (2*M_PI) ) * (2*M_PI); // convert to [-pi pi]

	// xy 
	double agx_r    = F_xy[0] * pos_r[0] + F_xy[1] * vel_r[0] + F_xy[2] * ( -1 * ipos_err[0] ) + F_xy[3] * pos[0] + F_xy[4] * v_g[0] + acc_r[0]; 
	double agy_r    = F_xy[0] * pos_r[1] + F_xy[1] * vel_r[1] + F_xy[2] * ( -1 * ipos_err[1] ) + F_xy[3] * pos[1] + F_xy[4] * v_g[1] + acc_r[1]; 

	double thr_agx = 6;	// threshold
	double thr_agy = 6; 

	if (agx_r < -thr_agx) agx_r = -thr_agx; 
	if (agx_r >  thr_agx) agx_r =  thr_agx; 
	if (agy_r < -thr_agy) agy_r = -thr_agy; 
	if (agy_r >  thr_agy) agy_r =  thr_agy; 

	// z
	double agz_r    = F_z[0] * pos_r[2] +  F_z[1] * vel_r[2] + F_z[2] * ( -1 * ipos_err[2] ) + F_z[3] * pos[2] + F_z[4] * v_g[2] + acc_r[2]; 
	double thr_agz	= 2; // threshold

	if (agz_r < -thr_agz) agz_r = -thr_agz; 
	if (agz_r >  thr_agz) agz_r =  thr_agz; 

	double a_g_r[3] = {agx_r, agy_r, agz_r};

	//-- the command conversion and innner loop control used in pixhawk
	
	double SIGMA  = 0.000001; 

	Vector3d thrust_sp(agx_r, agy_r, agz_r - g);
		
	att_sp.yaw_body		=  psi_r; 
    
	// calcuate attitude setpoint from thrust vector
	Vector3d thrust_sp_fixed( thrust_sp[0], thrust_sp[1], thrust_sp[2] );
	double thrust_abs_fixed	= thrust_sp_fixed.norm() ;  

	Vector3d body_z(0, 0, 0);
	if ( thrust_abs_fixed > SIGMA)
	{
		for (int i=0; i<3; i++)
		{
			body_z(i)	= -thrust_sp_fixed(i) / thrust_abs_fixed;
		}
	}
	else
	{
		// no thrust, set Z axis to safe value 
		body_z(2)		=   1.0;
	}
    
	// vector of desired yaw direction in XY plane, rotated by PI/2
	Vector3d y_C( -sin(psi_r), cos(psi_r), 0 );
    
	Vector3d body_x(0, 0, 0);
	if ( abs( body_z(2) ) > SIGMA )
	{
        
		// desired body_x axis, orthogonal to body_z
		body_x      =   y_C.cross(body_z);
        
		// keep nose to front while inverted upside down
		if ( body_z(2) < 0.0 )
		{
			body_x  =   -body_x;
		}
        
		body_x      =   body_x / body_x.norm();
	}
	else
	{
		// desired thrust is in XY plane, set X downside to construct correct matrix,
		// but yaw component will not be used actually
		//body_x      =   zeros(3,1);
		body_x(2)   =   1.0;     
	}
    
	// desired body_y axis
	Vector3d body_y(0, 0, 0);
	body_y		=  body_z.cross(body_x);
    
    
	// fill rotation matrix
	Matrix3d R; 
	R.col(0)	= body_x;
	R.col(1)	= body_y;
	R.col(2)	= body_z; 

    
	// copy rotation matrix to attitude setpoint topic
	for ( int i=0; i<3; i++ )
		for (int j=0; j<3; j++ )
			att_sp.R_body[i][j]	=   R(i,j);
    
	// calculate euler angles, for logging only, must not be used for control
	// https://stackoverflow.com/questions/33895970/about-eulerangles-conversion-from-eigen-c-library 

	Vector3d euler; 
	matrix_to_euler( R, euler(0), euler(1), euler(2) ); 
	att_sp.roll_body    =   euler(0);
	att_sp.pitch_body   =   euler(1);
	// yaw already used to construct rotation matrix, but actual rotation matrix can have different yaw near singularity
    
	// ********************************************************************
	//  attitude control
	struct vehicle_attitude_setpoint_s v_att_sp    =   att_sp;
    
	// rotation matrix in _att_sp is not valid, use euler angles instead 
	Matrix3d R_sp; 
	matrix_from_euler( v_att_sp.roll_body, v_att_sp.pitch_body, v_att_sp.yaw_body, R_sp); 
    
	// rotation matrix for current state
	matrix_from_euler( phi, tht, psi, R );
    

	// all input data is ready, run controller itself 
	// try to move thrust vector shortest way, because yaw response is slower than roll/pitch
	Vector3d R_z        =   R.col(2);  
	Vector3d R_sp_z     =   R_sp.col(2);
    
	// axis and sin(angle) of desired rotation 
	Vector3d e_R        =   R.transpose() * R_z.cross(R_sp_z); 
 
	// calculate angle error 
	double e_R_z_sin    =   e_R.norm();
	double e_R_z_cos	=   R_z.dot(R_sp_z);
    
	// calculate weight for yaw control
	double yaw_w        =   R_sp(2, 2) * R_sp(2, 2);
    
	// calculate rotation matrix after roll/pitch only rotation
	Matrix3d R_rp; 
	if ( e_R_z_sin > 0.0 ) 
	{
		// get axis-angle representation 
		double e_R_z_angle	=   (double) atan2( e_R_z_sin, e_R_z_cos );

		Vector3d e_R_z_axis =   e_R / e_R_z_sin;

		e_R					=  e_R_z_axis * e_R_z_angle;

		// cross product matrix for e_R_axis 
		Matrix3d e_R_cp; 
		e_R_cp.setZero(); 
		e_R_cp(0, 1)		=  -e_R_z_axis(2);
		e_R_cp(0, 2)		=   e_R_z_axis(1);
		e_R_cp(1, 0)		=   e_R_z_axis(2);
		e_R_cp(1, 2)		=  -e_R_z_axis(0);
		e_R_cp(2, 0)		=  -e_R_z_axis(1);
		e_R_cp(2, 1)		=   e_R_z_axis(0);

		// rotation matrix for roll/pitch only rotation 
		Matrix3d eye3;
		eye3.setIdentity(); 
		R_rp = R * ( eye3 + e_R_cp * e_R_z_sin + e_R_cp * e_R_cp * ( 1.0 - e_R_z_cos ) );
	}

	else 
	{
		// zero roll/pitch rotation 
		R_rp = R;
	}
    
	// R_rp and R_sp has the same Z axis, calculate yaw error 
	Vector3d R_sp_x  =   R_sp.col(0);  
	Vector3d R_rp_x  =   R_rp.col(0); 
	
	e_R(2) = atan2( R_rp_x.cross(R_sp_x).dot(R_sp_z) , R_rp_x.dot(R_sp_x) ) * yaw_w;

	if ( e_R_z_cos < 0.0 )
	{
	 	// for large thrust vector rotations use another rotation method:
	 	// calculate angle and axis for R -> R_sp rotation directly
		Eigen::Quaterniond q;

		q	= R.transpose() * R_sp;

		Vector3d e_R_d = q.vec(); 
	 	e_R_d.normalize();
		e_R_d *= 2.0f * atan2f( e_R_d.norm(), q.x() );
	 
	 	// use fusion of Z axis based rotation and direct rotation */
	 	double direct_w = e_R_z_cos * e_R_z_cos * yaw_w;
	 	e_R = e_R * (1.0f - direct_w) + e_R_d * direct_w;
	}

	Vector3d angle_error;  
	angle_error(0)  =   e_R(0);
	angle_error(1)  =   e_R(1);
	angle_error(2)  =   e_R(2) - floor( ( e_R(2) + M_PI ) / ( 2 * M_PI ) ) * ( 2 * M_PI ); // convert to [-pi pi]
    
	// For inner loop control
	double u_p		=   Ixx * ( F_phi[0] * (-1) * angle_error(0) + F_phi[1] * omg[0] );
	double u_q      =   Iyy * ( F_tht[0] * (-1) * angle_error(1) + F_tht[1] * omg[1] ); 
	double u_r      =   Izz * ( F_psi[0] * (-1) * angle_error(2) + F_psi[1] * omg[2] );
	double u_z      =   m * (-1) * thrust_sp.norm();

	// ESC control
	Vector4d u(u_p, u_q, u_r, u_z); 
	Vector4d omega2 = u_omega2 * u;   

	// make sure the (spinning rate)^2 is not negative
	if ( omega2(0) < 0 ) { omega2(0)=0; }
	if ( omega2(1) < 0 ) { omega2(1)=0; }
	if ( omega2(2) < 0 ) { omega2(2)=0; }
	if ( omega2(3) < 0 ) { omega2(3)=0; }

	Vector4d omega; 
	for ( int i=0; i<4; i++ ) { omega(i) = sqrt(omega2(i)); }

	//-- control inputs to the ESC
	// calculate the PWM signal for the motors based on first order approximation
	double uin[4]; 
	uin[0] = ( omega(0) - Kw1[1] ) / Kw1[0]; 
	uin[1] = ( omega(1) - Kw1[1] ) / Kw1[0];
	uin[2] = ( omega(2) - Kw2[1] ) / Kw2[0];
	uin[3] = ( omega(3) - Kw2[1] ) / Kw2[0];

	// check the range
	if ( uin[0] < 0 ) { uin[0] = 0; }
	if ( uin[0] > 1 ) { uin[0] = 1; }
	if ( uin[1] < 0 ) { uin[1] = 0; }
	if ( uin[1] > 1 ) { uin[1] = 1; }
	if ( uin[2] < 0 ) { uin[2] = 0; }
	if ( uin[2] > 1 ) { uin[2] = 1; }
	if ( uin[3] < 0 ) { uin[3] = 0; }
	if ( uin[3] > 1 ) { uin[3] = 1; }
	
	for (int i=0; i<4; i++) m_uin[i] = uin[i];

	return 0;
}


/*************************************************************************************
/*	Name:		SS
/*	Function:	lumped transformation matrix for rotation kinematic, (Cai's book, pp. 33) 			
/*	Parameter:	
/*              phi ----------- roll angle 
/*              tht ----------- pitch angle
/*              SS ------------ the lumped transformation matrix
/*	Return:		0 -- Succeed
/**************************************************************************************/
int QROTORCTRL::SS(double phi, double tht, clsMatrix &SS)
{
	double *pSS = SS.GetP();  
	pSS[0] = 1;
	pSS[1] = 0;
	pSS[2] =-sin(tht);
	pSS[3] = 0;
    pSS[4] = cos(phi);
	pSS[5] =-sin(phi)*cos(tht);
	pSS[6] = 0;
	pSS[7] =-sin(phi);
	pSS[8] = cos(phi)*cos(tht);

	return 0;
}