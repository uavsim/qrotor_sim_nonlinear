/*
 * libQrotorSim.h
 *
 *  Created on: 27 July 2017
 *      Author: Lin Feng
 *
 * Coded by Lin Feng    Modeled by Cui Jinqiang    Directed by Ben M. Chen
 * Copyright 2017 research UAV team, National University of Singapore
 * Updated: July 2017
 *
*/

#ifndef DEFQROTORCTRL_
#define DEFQROTORCTRL_

#include "matrix.h"
#include <random>

#include "libQrotorDyn.h"
#include "vehicle_attitude_setpoint.h"

#include "eigen/Eigen/core"
#include "eigen/Eigen/Geometry" 
using namespace Eigen; 

//-- Reference to the flight controller
struct QROTOR_REF    
{
	double p_x_r;		// [p_x_r, p_y_r, p_z_r] ---------- position in ground-axis, m
	double p_y_r; 
	double p_z_r; 
	double v_x_r;		// [v_x_r, v_y_r, v_z_r] ---------- velocity in ground-axis, m/s
	double v_y_r;
	double v_z_r;
	double agx_r;		// [agx_r, agy_r, agz_r] ---------- acceleration in ground-axis, m/s^2
	double agy_r;
	double agz_r;
	double psi_r;		// psi_r -------------------------- heading angle, rad
	double r_r;         // r_r ---------------------------- heading angle rate: rad/s
}; 


class QROTORCTRL {

public:	

	QROTORCTRL(){ Init(); };
	~QROTORCTRL(){};

	int Init();										// initialize the class	
	int RunOneStep(QROTOR_REF ref, double xc[15]);	// run one step of control

	double m_uin[4];								// uin = [ail; ele; thr; rud] --------- current inputs to the ESC

protected:
	
	int Body2NED(double phi, double tht, double psi, clsMatrix &R);					// rotation matrix transforming body-axis vector to NED-axis vector
	int matrix_from_euler(double phi, double tht, double psi, Eigen::Matrix3d &R);	// rotation matrix transforming body-axis vector to NED-axis vector
	int matrix_to_euler(Eigen::Matrix3d R, double &phi, double &tht, double &psi);  // compute Euler angles from the rotation matrix
	int SS(double phi, double tht, clsMatrix &SS);									// lumped transformation matrix for rotation kinematic


protected:	
	//-- simulation parameters
	double m_ts;	// step size for model simulation, sec
	double m_Ts;	// step size for control simulation, sec

	//-- model parameters
	double g;		// gravitational acceleration in Singapore, m/s^2
	double m;		// total mass of helicopter, kg

	double Kt1;		// Thrust coefficient, counter clock wise
	double Kt2;		// Thrust coefficient, clock wise
	double Kq;		// Drag coefficient

	double Kw1[2];	// the gain of PWM to ration speed, counter clock wise. 
	double Kw2[2];  // the gain of PWM to ration speed, clock wise

	double Lm;		// the length of the arm of the platform, m

	double Ixx;		// rolling moment of inertia, kg*m^2  (real measured value)
	double Iyy;		// pitching moment of inertia, kg*m^2 (ream measured value)
	double Izz;		// yawing moment of inertia, kg*m^2   (real measured value)

	//-- for ESC control
	Matrix4d omega2_u;   
	Matrix4d u_omega2;

	// attitude control law
	double ksi_phi; 
	double wn_phi;	
	double F_phi[2];
	double G_phi;
 
	double ksi_tht; 
	double wn_tht; 
	double F_tht[2];
	double G_tht;

	// xy outer-loop controller
	double wn_xy;
	double sigma_xy;
	double ki_xy;
	double eps_xy;
	double F_xy[5];

	// z outer-loop controller
	double wn_z;
	double sigma_z;
	double ki_z;
	double eps_z;
	double F_z[5];

	// psi controller design
	double ksi_psi; 
	double wn_psi;  
	double F_psi[2];
	double G_psi;

	//-- integration the error of position and heading angle 
	double ipos_err[3];
	double ipsi_err; 

	//-- set points of attitude used in Pixhawk
	struct vehicle_attitude_setpoint_s att_sp; 
};

#endif